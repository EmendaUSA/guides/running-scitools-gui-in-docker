# ===========================================================
# Emenda USA: DockerFile to setup/execute SciTools Understand
# ===========================================================

# ===========================================================
# MUST HAVE: Basic System Detail, we start from ubuntu.
# -----------------------------------------------------------
FROM ubuntu:22.04
ENV LANG en_US.UTF-8

# ===========================================================
# MUST HAVE: Parameter(s) and Variable(s)
# -----------------------------------------------------------
# The tgz file that is used to install understand. This file
# must be located in the same directory that the DockerFile
# when you run 'docker build ...'
ARG SCITOOLS_ZIP=Understand-CLI.64bit.tgz
# -----------------------------------------------------------
# UID and GID if needed (useful if you want to integrate 
# better user control)
ARG UID=1000
ARG GID=1000
# -----------------------------------------------------------
# Directory where SciTools will be installed
ENV INSTALLDIR=/scitools_dir
# -----------------------------------------------------------
# User name that will be created to run the pipeline
ENV UNAME=scitools_user
# -----------------------------------------------------------

# ===========================================================
# Update packages from the OS
# -----------------------------------------------------------
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get upgrade -y
# -----------------------------------------------------------

# ===========================================================
# Package Required to run SciTools Undertand
# -----------------------------------------------------------
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \
libxcb-xinerama0 libxkbcommon-x11-0 libxcb-shape0 libxcb-cursor0 \
libxcb-icccm4 libxcb-image0 libxcb1 libxcb-keysyms1 \
libxcb-render-util0 libglib2.0-dev language-pack-en-base dnsutils \
xorg qt6-base-dev
# -----------------------------------------------------------

# ===========================================================
# Adding git and zip as we will need to download scitools-essential,
# a set of useful scripts to help with the build.
# -----------------------------------------------------------
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y git zip

# ===========================================================
# Optional. Package to run gcc, make, cmake and bear
# -> You can replace by your own compiler chain here!
# -> bear is only useful if you don't use CMAKE
# -----------------------------------------------------------
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \ 
build-essential bear dos2unix

# ===========================================================
# Optional. Package(s) used for debugging. (ifconfig, ping, vi, host) 
# -----------------------------------------------------------
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \
iputils-ping net-tools vim curl
# -----------------------------------------------------------

# ===========================================================
# Creation of user that will run the pipeline 
# -----------------------------------------------------------
RUN groupadd -g $GID -o $UNAME
RUN useradd -r -m -u $UID -g $GID -o $UNAME
# -----------------------------------------------------------

# ===========================================================
# Optional: Make sure user able to run "sudo" (3 lines)
# Note: we default the user password to 'pass'
# You probably want to remove this in production.
# -----------------------------------------------------------
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y sudo
RUN echo "${UNAME}:pass" | chpasswd
RUN adduser $UNAME sudo
# -----------------------------------------------------------

# ===========================================================
# Creating an install directory
# -----------------------------------------------------------
RUN mkdir $INSTALLDIR
RUN chown -R $UNAME:$UNAME $INSTALLDIR

# ===========================================================
# Installing SciTools
# -----------------------------------------------------------
USER ${UNAME}
WORKDIR $INSTALLDIR
RUN curl --output $SCITOOLS_ZIP --url https://s3.amazonaws.com/builds.scitools.com/all_builds/b1188/Understand/Understand-6.5.1188-Linux-64bit.tgz
RUN tar -xvzf $SCITOOLS_ZIP
RUN rm $SCITOOLS_ZIP

# ===========================================================
# Turning over to end-user
# -----------------------------------------------------------
USER ${UNAME}

# ===========================================================
# Setup Path 
# -----------------------------------------------------------
ENV PATH=$PATH:/home/$UNAME/.local/bin
ENV PATH=$PATH:$INSTALLDIR/scitools/bin/linux64
# -----------------------------------------------------------

# ===========================================================
# Setup DISPLAY
# -----------------------------------------------------------
ENV DISPLAY=host.docker.internal:0
# -----------------------------------------------------------


# ===========================================================
# Finalizing setup... by setting up where we will start.
WORKDIR /home/$UNAME