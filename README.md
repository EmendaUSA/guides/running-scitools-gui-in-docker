# SciTools Docker Images (for GUI USE)

Objectif: Run SciTools Understand in a Docker Image based on Ubuntu

## STEP 1. Build the image:

We use the [provided Dockerfile](./Dockerfile.allinone). To build:

```
docker build -t scitools-runner-gui -f Dockerfile.allinone .
```

You may want to add `--no-cache` if you want to refresh it completely.

## STEP 2. Run the image:

### You host is WINDOWS:

- [ ] Install a X Server on your machine. I use [MobaXTerm](http://mobaxterm.mobatek.net/download.html)
- [ ] Run the docker using `docker run --rm -ti --name scitools-runner-gui-testing scitools-runner-gui bash`
- [ ] You are at a bash screen
- [ ] Run `understand`
- [ ] **Make sure to run `und -deregisterlicensecode` before closing the docker container.**

### Your host is LINUX:

- [ ] Run the docker using `docker run --env="DISPLAY" --net=host  -v /tmp/.X11-unix/:/tmp/.X11-unix/ --rm -ti --name scitools-runner-gui-testing scitools-runner-gui bash`
- [ ] You are at a bash screen
- [ ] Run `understand`
- [ ] **Make sure to run `und -deregisterlicensecode` before closing the docker container.**

## Saving your work? How to?

### Copy file(s) in and out of Docker

You can copy file(s) in and out of your docker using `docker cp`. 

`docker cp scitools-runner-gui-testing:(PATH ON DOCKER CONTAINER) (PATH ON HOST)`

### Use a Docker Volume

Alternatively, you can create a volume to persist works between execution(s).

To create the volume:

`docker volume create scitools-runner-gui-hdd`

Then, use it by adding `-v scitools-runner-gui-hdd:/home/scitools_user` to your docker execution line.

Example:
```
docker run --rm -ti --name scitools-runner-gui-testing -v scitools-runner-gui-hdd:/home/scitools_user scitools-runner-gui bash
```
Now your home directory will be stored between execution.

To access the files in the volume, go to those locations:

- Windows: `\\wsl$\docker-desktop-data\data\docker\volumes`
- Linux: `/var/lib/docker/volume`

